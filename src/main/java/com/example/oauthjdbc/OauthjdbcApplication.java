package com.example.oauthjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@SpringBootApplication
@EnableJpaAuditing

public class OauthjdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthjdbcApplication.class, args);
	}

}
