package com.example.oauthjdbc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;


@EnableResourceServer
@Configuration
public class ResourcesServerConfiguration extends ResourceServerConfigurerAdapter {

//    @Bean
//    @ConfigurationProperties(prefix="spring.datasource")
//    public DataSource ouathDataSource(){return DataSourceBuilder.create().build();}

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {

        //TokenStore tokenStore=new JdbcTokenStore(ouathDataSource());
        resources.resourceId("oauth2-resource");

    }

    @Override

    public void configure(HttpSecurity http) throws Exception {


        http
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/**").access("hasRole('ROLE_operator')")
                .antMatchers(HttpMethod.POST, "/**").access("#oauth2.hasScope('ROLE_admin')")
                .antMatchers(HttpMethod.PATCH, "/**").access("#oauth2.hasScope('ROLE_admin')")
                .antMatchers(HttpMethod.PUT, "/**").access("#oauth2.hasScope('ROLE_operator')")
                .antMatchers(HttpMethod.DELETE, "/**").access("#oauth2.hasScope('ROLE_admin')")
                .and()

                .headers().addHeaderWriter((request, response) -> {
            response.addHeader("Access-Control-Allow-Origin", "*");
            if (request.getMethod().equals("OPTIONS")) {
                response.setHeader("Access-Control-Allow-Methods", request.getHeader("Access-Control-Request-Method"));
                response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
            }
        });
    }
}